import React, {component} from 'React';
class Post extends component{
    render() {
        return(
            <aside className="post">
                <div className="post-user">
                    <div className="post-user-avatar">
                        <img src="https://media.wired.com/photos/5cdefc28b2569892c06b2ae4/master/w_2560%2Cc_limit/Culture-Grumpy-Cat-487386121-2.jpg" alt="profile picture"></img>
                    </div>
                    <div className="post-user-name">
                        <span>Komal</span>
                    </div>
                </div>
                <div className="post-image">
                    <div className="post-image-source">
                        <img src="https://static.boredpanda.com/blog/wp-content/uploads/2020/01/cat-meme-joke-1.jpg" alt="post image"></img>
                    </div>
                    <div className="post-image-caption">
                        <strong>Heyaa</strong>
                    </div>
                </div>
            </aside>
        );
    }
}
export default Post;