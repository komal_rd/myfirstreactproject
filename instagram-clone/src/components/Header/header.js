import React from "react";
import "./header.scss";

class Header extends React.Component{
    render(){
        return(
            <nav className="nav">
                    <a className="nav-menus logo" href="/">
                        <img src="https://scx2.b-cdn.net/gfx/news/hires/2019/instagram.jpg" alt="Instagram logo" className="nav-menus logoimage"></img>
                    </a>
                    <div className="nav-menus">Instagram</div>
                    <img src="https://images.vexels.com/media/users/3/147102/isolated/preview/082213cb0f9eabb7e6715f59ef7d322a-instagram-profile-icon-by-vexels.png" alt="profile view" className="nav-menus profileimage"></img>
            </nav>
        );
    }
}
export default Header;
